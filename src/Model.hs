-- | This module contains the data types
--   which represent the state of the game
module Model where
import System.Random
-- Ik wil de Random importeren maar ik kon het niet want hij zei dat hij alle packges die ik nu heb unloaden
import Data.Set
import Graphics.Gloss.Interface.IO.Game

data InfoToShow = ShowNothing
                | ShowANumber Int
                | ShowAChar   Char

nO_SECS_BETWEEN_CYCLES :: Float
nO_SECS_BETWEEN_CYCLES = 5

data GameState = GameState{ asteroids :: [Asteroid]
                          , ufos      :: [Ufo]
                          , bullets   :: [Bullet]
                          , spaceship :: SpaceShip
                          , coins     :: [Coin]
                          , score     :: Int
                          , keys      :: Set Key
                          }


data Asteroid = Asteroid AsPoint DirVector Size

data Ufo = Ufo AsPoint DirVector
          | RandomUfo-- dit wordt gebruikt pas nadat we Random library kunnen gebruiken 

data SpaceShip = SpaceShip AsPoint DirVector

data Coin = Coin AsPoint Int

type AsPoint = (Float, Float)

type DirVector = (Float, Float)

type Size = Int

data Bullet = Bullet AsPoint DirVector Bool

aantalFrames:: Float -- hoeveel frames gebruikt een bullets om te bewegen
aantalFrames = 10
snelheid    :: Float -- snelheid van de bullets 
snelheid = 30

initialState :: GameState
initialState = GameState [] 
                         [(Ufo (200,230) (0,10)),(Ufo (100,100) (0,300))]
                         [(Bullet (15,0) (20,0) False)] 
                         (SpaceShip (20,20) (0, 5)) 
                         [Coin (300,300) 10] 
                         0
                         empty
{- initialState1 :: GameState
initialState1 = GameState [] [] [] (SpaceShip (0, 0) (0, 0)) [] 0 
 createRandomCoordinates :: (Float,Float) -> IO AsPoint  
createRandomCoordinates (w,h) = do x<- randomRIO(0,w)
                                   y<- randomRIO (0,h)
                                   return ( x, y)  -}
{-createRandomCoordinates :: (Float, Float) -> IO AsPoint
createRandomCoordinates (w, h) = do x <- randomRIO (0, w)
                                    y <- randomRIO (0, h)
                                    return (x, y)-}