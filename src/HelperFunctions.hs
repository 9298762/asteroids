module HelperFunctions where

import Model
import Graphics.Gloss
import Graphics.Gloss.Data.Vector

rotateVector :: Float -> DirVector -> DirVector
rotateVector f (x,y) = ( x * (cos theta) - y * (sin theta) , x * (sin theta) + y * (cos theta) )
                 where theta = degreesToRadians f

degreesToRadians :: Float -> Float
degreesToRadians f = f * pi / 180

radiansToDegrees :: Float -> Float
radiansToDegrees f = f * 180 / pi

{- rotateShape:: Shape -> Float -> Shape
rotateShape (Polygon [(x0, y0), (x1, y1), (cx, cy), (x2, y2), (x3, y3)]) f = Polygon [(y0, (-1)*x0),
                                                                                      (y1,(-1)*x1),
                                                                                      (cy,cx),
                                                                                      (y2,(-1)*x2),
                                                                                      (y3,(-1)*x3)] -}

calculateAngle :: DirVector -> Float
calculateAngle dv@(dx, dy) = 90 - radiansToDegrees (argV dv)

abs' :: DirVector -> Float
abs' (dx, dy) = sqrt(dx*dx + dy*dy)

ssRotate :: Picture -> DirVector -> AsPoint -> Picture
ssRotate pic dv p@(px, py)= Translate px' py' (Rotate (calculateAngle dv)(Translate (-px') (-py') pic))
                     where (px', py') = (px + 15, py + 24)
                     -- 15 is approx. 0.5 * width letter
                     -- 24 is approx. 0.5 * height letter