-- | This module defines how to turn
--   the game state into a picture
module View where

import HelperFunctions (calculateAngle, ssRotate)
import Graphics.Gloss
import Model
import Controller

view :: GameState -> IO Picture
view = return . drawGameState

drawGameState :: GameState -> Picture
drawGameState gs = case gs of
    GameState a u@[(Ufo (x0, y0) (dx0, dy0)), (Ufo (x1, y1) (dx1, dy1))] b@([(Bullet (bx,by) (bdx,bdy) True)]) (SpaceShip (x, y) (dx, dy)) [(Coin (z, t) r)] sc keys ->
                 Pictures [drawShip (SpaceShip (x, y) (dx, dy)),
                           drawBullet b,
                           drawUfo u,
                           drawCoin (Coin (z, t) r)]
    GameState a u@[(Ufo (x0, y0) (dx0, dy0)), (Ufo (x1, y1) (dx1, dy1))] b@([(Bullet (bx,by) (bdx,bdy) False)]) (SpaceShip (x, y) (dx, dy)) [(Coin (z, t) r)] sc keys->
                 Pictures [drawShip (SpaceShip (x, y) (dx, dy)),
                           drawUfo u,
                           drawCoin (Coin (z, t) r)]
    
      
drawShip:: SpaceShip -> Picture
drawShip s@(SpaceShip (x,y) (dx, dy)) = Pictures [ssRotate
                                                         (Color white(Translate x y(Scale 2.0 2.0 (Polygon [(15, 0), (-15, 10), (-10,0), (-15, -10), (15, 0)]))))
                                                         (dx, dy)
                                                         (x, y)]

drawBullet :: [Bullet] -> Picture
drawBullet [] = Blank
drawBullet bullets@((Bullet (x,y) (dx, dy) _):rest) = Pictures [drawOneBullet b | b <- bullets]
                                                            where 
                                                               drawOneBullet (Bullet (x,y) (dx, dy) _)= Color red(Scale 2.0 2.0 (Line [(x,y), (dx,dy)]))



drawUfo:: [Ufo] -> Picture
drawUfo [] = Blank
drawUfo us@(Ufo (x0,y0) (dx0,dy0):rest) = Pictures[ drawOneUfo u | u <- us ]
                                                where 
                                                    drawOneUfo (Ufo (x0,y0) (dx0,dy0)) = Rotate (atan (dx0/dy0)) 
                                                                                         (Color red(Translate x0 y0 (Scale 2.0 2.0 (Polygon [(-6, 0), (-3, -5), (2,-5), (5, 0), (0, 5)]))))

drawCoin :: Coin -> Picture
drawCoin c@(Coin (z,t) r)= Pictures [Color yellow(Translate z t (Scale 0.7 0.7 (thickCircle 10 10)))]



-- oude code :
{- viewPure :: GameState -> Picture
viewPure (GameState a [(Ufo (x0,y0) (dx0,dy0)),(Ufo (x1,y1) (dx1,dy1))] b (SpaceShip (x,y) (dx, dy)) [(Coin (z,t) r)] sc) 
                        = Pictures  [ (Color yellow(Translate x1 y1 (Scale 0.4 0.4 (Text "O")))),
                                       Rotate (acos (dy/dx)) (Color white(Translate x y(Scale 2.0 2.0 (Polygon [(15, 0), (-15, 10), (-10, 0), (-15, -10), (15, 0)])))),
                                     Rotate (atan (dx0/dy0)) (Color red(Translate x0 y0 (Scale 0.4 0.4 (Text "O")))),
                                     (Color yellow(Translate z t (Scale 0.7 0.7 (thickCircle 10 10))))]
                                     -}


{-view1 :: GameState -> IO Picture
view1 gs = return $ Pictures
                             [ Color (if buttonClicked gs then green else red) (rectangleSolid 100 50)
                             , Translate (-40) (-10) $ Text "Click me!"
                             ]-}

