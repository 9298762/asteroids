module Main where

import Controller
import Model
import View

import Graphics.Gloss.Interface.IO.Game

main :: IO ()
main = do
   
       playIO (InWindow "Asteroids" (1000, 700) (10, 10)) 
              black 
              20 
              initialState
              view
              input
              static


-- To DO :
{-
 Bullet , 
 bullet uit de spaceship,
start pagina, 
Ufo's Random,
 highscore(uit textfile kan lezen),
 tijd,
 ontploffen van de spaceship en van de UFo's, 
 score, 
 coins 
-}