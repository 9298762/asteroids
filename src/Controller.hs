-- | This module defines how the state changes
--   in response to time and user input
module Controller where

import Model
import HelperFunctions (rotateVector)
import Data.Set
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game

-- | Handle one iteration of the game
static :: Float -> GameState -> IO GameState
static f = return . iteration

iteration :: GameState -> GameState
iteration gs@(GameState a u b (SpaceShip v@(x,y) dv@(dx, dy)) c sc keys)
         | member (Char 'w') keys = GameState a u b (SpaceShip (x+dx, y+dy) dv) c sc keys
         | member (Char 'a') keys = GameState a u b (SpaceShip v (rotateVector 10 dv)) c sc keys
         | member (Char 'd') keys = GameState a u b (SpaceShip v (rotateVector (-10) dv)) c sc keys
         | otherwise              = gs

-- | Handle user input
input :: Event -> GameState -> IO GameState
input e gstate = return (update e gstate)

-- dit wil ik nog gebruiken in de start pagina maar ik wil nog uitzoeken hoe we van de eerste window naar de tweede moeten gaan
{- input1 :: Event -> GameState -> IO GameState
input1 e gs = return (updateWindow e gs) -}


update :: Event -> GameState -> GameState
update (EventKey (Char 'a') Down _ _) gs@(GameState a u b ss c sc keys) 
                                     = GameState a u b ss c sc (insert (Char 'a') keys)
update (EventKey (Char 'a') Up _ _) gs@(GameState a u b ss c sc keys) 
                                     = GameState a u b ss c sc (delete (Char 'a') keys)
update (EventKey (Char 'w') Down _ _) gs@(GameState a u b ss c sc keys)
                                     = GameState a u b ss c sc (insert (Char 'w') keys)
update (EventKey (Char 'w') Up _ _) gs@(GameState a u b ss c sc keys)
                                     = GameState a u b ss c sc (delete (Char 'w') keys)
update (EventKey (Char 'd') Down _ _) gs@(GameState a u b ss c sc keys) 
                                     = GameState a u b ss c sc (insert (Char 'd') keys)
update (EventKey (Char 'd') Up _ _) gs@(GameState a u b ss c sc keys) 
                                     = GameState a u b ss c sc (delete (Char 'd') keys)


update (EventKey (Char 'b') Down _ _) gs@(GameState a u [(Bullet (bx,by) (bdx,bdy) False)] s c sc keys) 
                                     = GameState a u [(Bullet (bx,by) (bdx,bdy) True)] s c sc keys
                                                 
update _ gs = gs

bulletVisible :: [Bullet] -> [Bullet]
bulletVisible [] = []
bulletVisible ((Bullet (bx, by) (bdx, bdy) _) : rest) = Bullet (bx, by) (bdx, bdy) True : bulletVisible rest

makeNewBullet :: GameState -> GameState
makeNewBullet gs@( GameState{bullets = b , spaceship = SpaceShip (x,y) (dx,dy)})=
    gs{ bullets = ((Bullet (x,y) (dx,dy) True) : b) }

{- -- update van de window 
updateWindow :: Event -> GameState -> GameState
updateWindow (EventKey (MouseButton LeftButton) Down _ _) gs@(GameState a u b (SpaceShip (x,y) (dx,dy)) c sc )
                                     = GameState a u b (SpaceShip (x,y) (dx,dy)) c sc
updateWindow _ gs = gs

 -}
